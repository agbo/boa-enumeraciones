//
//  main.m
//  enumeraciones
//
//  Created by Fernando Rodríguez Romero on 21/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        
        // NSString
        [@"uno\ndos\ntres" enumerateLinesUsingBlock:^(NSString *line, BOOL *stop) {
            
            static int index = 0;
            NSLog(@"La linea %d contiene %@", index, line);
            index++;
        }];
        
        // Array
        [@[@1,@2,@"tres"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            NSLog(@"El objeto número %lu es %@", (unsigned long)idx, obj);
        }];
        
        
        // Dict
        [@{@"Star Wars" : @"Williams", @"Born Free" : @"Barry"}
         enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
             
             NSLog(@"La BSO de %@ fue compuesta por %@", key, obj);
         }];
        
        
        
        
        
        
        
        
        
        
        
        
    }
    return 0;
}
